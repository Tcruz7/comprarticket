package vueling;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class comprarTicket {

	WebDriver driver = new ChromeDriver();

	@Given("^I enter in the \"([^\"]*)\" page$")
	public void iEnterInThePage(String title) throws Throwable {

		driver.get("https://tickets.vueling.com");
		driver.manage().window().maximize();
		String titlePage = driver.getTitle();
		Assert.assertEquals(title, titlePage);

		System.out.println("We are in: " + title + "\n");
	}

	@Given("^I insert the place of departure$")
	public void i_insert_the_place_of_departure() throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		// driver.findElement(By.id("idCookiePolicyCloseOption")).click();
		driver.findElement(
				By.id("AvailabilitySearchInputSearchView_TextBoxMarketOrigin1"))
				.click();
		driver.findElement(
				By.xpath("/html/body/form/div/div[1]/div[6]/div[1]/div[3]/div[1]/div[1]/div[2]/div/div/div[1]/div/div[2]/div/div/div/ul/li[11]/a"))
				.click();
		WebElement element = driver.findElement(By
				.id("AvailabilitySearchInputSearchView_TextBoxMarketOrigin1"));
		String elementval = element.getAttribute("value");
	
		System.out.println("Selected starting place: " + elementval + "\n");

	}

	@Given("^I insert the destination$")
	public void iInsertTheDestination() throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(
				By.id("AvailabilitySearchInputSearchView_TextBoxMarketDestination1"))
				.click();
		driver.findElement(
				By.xpath("/html/body/form/div/div[1]/div[6]/div[1]/div[3]/div[1]/div[1]/div[2]/div/div/div[2]/div/div[2]/div/div/div[1]/ul/li[67]/a"))
				.click();
		WebElement element = driver
				.findElement(By
						.id("AvailabilitySearchInputSearchView_TextBoxMarketDestination1"));
		String elementval = element.getAttribute("value");
		System.out.println("Selected destination: " + elementval + "\n");

	}

	@Given("^I select the departure date (\\d+)$")
	public void i_select_the_departure_date(String dia) throws Throwable {
		WebDriverWait wait2 = new WebDriverWait(driver, 10);

		// String today = getCurrentDay();
		System.out.println("Departure day: " + dia + "\n");

		WebElement datePanel = wait2.until(ExpectedConditions
				.elementToBeClickable(By.id("datePickerContainer")));
		List<WebElement> columns = datePanel.findElements(By.tagName("td"));
		for (WebElement cell : columns) {

			if (cell.getText().equals(dia)) {
				cell.click();
				break;
			}
		}
	}

	@Given("^I select the return date (\\d+)$")
	public void i_select_the_return_date(String dia) throws Throwable {

		WebDriverWait wait2 = new WebDriverWait(driver, 10);

		WebElement datePanel = wait2.until(ExpectedConditions
				.elementToBeClickable(By.id("datePickerContainer")));

		List<WebElement> columns = datePanel.findElements(By.tagName("td"));
		for (WebElement cell : columns) {

			if (cell.getText().equals(dia)) {
				// if (cell.getText().equals(today)) {
				cell.click();
				break;
			}
		}

		System.out.println("return day: " + dia + "\n");
	}

	@Given("^I select tow adults$")
	public void iSelectTowAdults() throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		WebElement element = driver.findElement(By
				.id("DropDownListPassengerType_ADT_2"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);

		System.out.println("Click tow adults OK..." + "\n");
	}

	@Given("^I select one children$")
	public void iSelectOneChildren() throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(
				By.xpath("/html/body/form/div/div[1]/div[6]/div[1]/div[3]/div[1]/div[1]/div[5]/div[2]/fieldset/select/option[2]"))
				.click();

		System.out.println("Click one children OK..." + "\n");

	}

	@When("^I click in search for flights$")
	public void iClickInSearchForFlights() throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		WebElement element = driver
				.findElement(By
						.id("AvailabilitySearchInputSearchView_btnClickToSearchNormal"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);

		System.out.println("Click search for flights OK..." + "\n");
	}

	@When("^select the outbound flight$")
	public void select_the_outbound_flight() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		WebElement element = driver
				.findElement(By
						.id("ControlGroupScheduleSelectView_AvailabilityInputScheduleSelectView_RadioButtonMkt1Fare1"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);

		System.out.println("Click outbound flight OK..." + "\n");
	}

	@When("^select the flight back$")
	public void select_the_flight_back() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		WebElement element = driver
				.findElement(By
						.id("ControlGroupScheduleSelectView_AvailabilityInputScheduleSelectView_RadioButtonMkt2Fare1"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);

		System.out.println("Click flight back OK..." + "\n");
	}

	@Then("^I click to continue$")
	public void iClickToContinue() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		WebElement element = driver
				.findElement(By
						.id("ControlGroupScheduleSelectView_AvailabilityInputScheduleSelectView_LinkButtonSubmit"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);

		System.out.println("Click to continue OK..." + "\n");

	}

	@Then("^I select the basic rate$")
	public void iSelectTheBasicRate() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		WebElement element = driver.findElement(By.id("buttonKeepBasic"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);

		System.out.println("Click to basic rate OK..." + "\n");

	}
}